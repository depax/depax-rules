# Rules

[![CircleCI](https://circleci.com/bb/depax/depax-rules.svg?style=svg)](https://circleci.com/bb/depax/depax-rules)
[![Todos](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-rules/latest/artifacts/0/shields/todos.svg)](#)
[![Features](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-rules/latest/artifacts/0/shields/features.svg)](#)
[![Coverage](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-rules/latest/artifacts/0/shields/features-coverage.svg)](#)
[![Documentation](https://img.shields.io/badge/documentation-🕮-brightgreen.svg?style=plastic)](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-rules/latest/artifacts/0/documentation/index.html)
[![Report](https://img.shields.io/badge/report-💣-brightgreen.svg?style=plastic)](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-rules/latest/artifacts/0/report)

## Installation

Install the package normally using NPM or Yarn.

```sh
yarn add @depax/rules
```

## Usage

A rule is a simple collection of conditions and actions, an example of creating and executing a rule is as follows;

```js
import Execute, { IReport, IRule } from "@depax/rules";

const rule: IRule = {
    actions: [
        { id: "SetArg", config: { arg: "message", value: "hello" } },
    ],
    conditions: [
        { id: "IsEqual", config: { actual: "@arg1", expected: true } },
    ],
};

const args = { arg1: true };
const report: IReport = Execute(rule, args);

if (report.success) {
    console.info("Successfully executed rule, message =", args.message);
    console.info(report);
} else {
    console.error("Failed to execute rule!");
    console.info(report);
}
```

Rules can also be executed via events and groups, this is done by creating the rule object, registering it to the
global collection with an ID, and then mapping an event and optionally a group to the rule;

```js
import { ExecuteEvent, IEventReport, IReport, IRule, MapRule, rules } from "@depax/rules";

const rule: IRule = {
    actions: [
        { id: "SetArg", config: { arg: "message", value: "hello" } },
    ],
    conditions: [
        { id: "IsEqual", config: { actual: "@arg1", expected: true } },
    ],
};

// Register the rules so the mapping can find it.
rules.set("my-rule", rule);

// Then we define a mapping, we can map the rule to multiple events if we wanted.
MapRule("my-rule", "my-event");

// We can apply a weight to the rule, so that it could be executed sooner or later.
MapRule("my-rule", "my-event-5", 25);

// Or we can also apply it to a group, within the event.
MapRule("my-rule", "my-event-5", 0, "my-group");

const args = { arg1: true };
let reports: IEventReport = ExecuteEvent("my-event", args);

// Or we can execute a specific group within the event;
reports = ExecuteEvent("my-event5", args, "my-group");
```

## Extending

The rules engine does not provide any conditions or actions out of the box other than the examples `IsEqual` condition
and `SetArg` action. Conditions and actions are simple callbacks which are registered in a map.

### Defining new Conditions

To define a new condition, simply create a function with the expected signature, and register it;

```js
import { deepEqual, equal } from "assert";
import { conditions, EConfigDescriptorType, EResponse, IDescriptor, IObject, ParseArgToken } from "@depax/rules";

const IsEqualDescriptor: IDescriptor = {
    config: {
        value1: {
            description: "The value we want to compare with.",
            required: true,
            type: EConfigDescriptorType.Any,
        },
        value2: {
            description: "The value that we are comparing to.",
            required: true,
            type: EConfigDescriptorType.Any,
        },
    },
    description: "Determines if two values match each other.",
};

async function IsEqual(args: IObject, config: IObject): Promise<EResponse> {
    // Make sure the config has the required properties provided.
    if (!(!!config.actual && !!config.expected)) {
        throw new Error("Missing the 'actual' and/or 'expected' properties in the config.");
    }

    // We use the *ParseArgToken* utility function to convert "@..." strings to the respective arg property, if
    // available.
    const actual = ParseArgToken(config.actual, args);
    const expected = ParseArgToken(config.expected, args);

    try {
        if (actual instanceof Object) {
            deepEqual(actual, expected);
        } else {
            equal(actual, expected);
        }

        return EResponse.Pass;
    } catch (err) {
        return EResponse.Fail;
    }

    // We return either a Pass, or Fail.
}

// Then we simply register the function globally;
conditions.set("IsEqual", {
    descriptor: IsEqualDescriptor,
    callback: IsEqual,
});
```

### Defining new Actions

To define a new action, is basically the same as conditions, just registered to a different collection;

```js
import { deepEqual, equal } from "assert";
import { actions, EConfigDescriptorType, EResponse, IDescriptor, IObject, ParseArgToken } from "@depax/rules";

const SetArgDescriptor: IDescriptor = {
    config: {
        arg: {
            description: "The name of the argument to add or update.",
            required: true,
            type: EConfigDescriptorType.String,
        },
        value: {
            description: "The value to apply to the argument.",
            required: true,
            type: EConfigDescriptorType.Any,
        },
    },
    description: "Set an argument value.",
};

async function SetArg(args: IObject, config: IObject): Promise<EResponse> {
    // Make sure the config has the required properties provided.
    if (!(!!config.arg && !!config.value)) {
        throw new Error("Missing the 'arg' and/or 'value' properties in the config.");
    }

    // We use the *ParseArgToken* utility function to convert "@..." strings to the respective arg property, if
    // available.
    const arg = ParseArgToken(config.arg, args);
    const value = ParseArgToken(config.value, args);

    args[arg] = value;
    return EResponse.Pass;

    // We return either a Pass, Fail, or Skip.
}

// Then we simply register the function globally;
actions.set("SetArg", {
    descriptor: SetArgDescriptor,
    callback: SetArg,
});
```

### Defining Events

There is a global map to define event details, however this serves as administrative and is not used within the rules
or execution.
