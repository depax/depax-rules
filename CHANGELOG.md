# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.6] - 2018-11-18
### Updated

- Amended args parser to use Data to allow for an breadcrumb like format.

## [1.0.5] - 2018-11-18
### Updated

- Amended so that conditions arrays are optional, to allow only action rules.
- Amended `Execute` so it will not error if the conditions array is not defined.

## [1.0.4] - 2018-11-03
### Updated

- Amended the rule status to return false if there was an error.

## [1.0.3] - 2018-11-03
### Updated

- Updated the execution of actions to capture any errors and report them.

## [1.0.2] - 2018-10-28
### Added

- Added optional 'values' property to the config descriptor to allow multiple options.

### Updated

- Amended number type label.

## [1.0.1] - 2018-10-24
### Updated

- Amended package script to use `prepublishOnly` instead of `prepare`.

## [1.0.0] - 2018-10-01
### Added

- Initial development

[unlicense]: http://unlicense.org/
[1.0.0]: https://bitbucket.org/depax/depax-rules/src/v1.0.0/
[1.0.1]: https://bitbucket.org/depax/depax-rules/src/v1.0.1/
[1.0.2]: https://bitbucket.org/depax/depax-rules/src/v1.0.2/
[1.0.3]: https://bitbucket.org/depax/depax-rules/src/v1.0.3/
[1.0.4]: https://bitbucket.org/depax/depax-rules/src/v1.0.4/
[1.0.5]: https://bitbucket.org/depax/depax-rules/src/v1.0.5/
[1.0.6]: https://bitbucket.org/depax/depax-rules/src/v1.0.6/

