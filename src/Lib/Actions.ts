/**
 * Provides the various types for the Action callbacks.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import { basename, join } from "path";
import { RuleCallback } from "../Rules";
import glob from "../Utils/Glob";
import { IDescriptor } from "./Interfaces";

/** A collection of the defined action callbacks. */
const actions: Map<string, {
    /** A descriptor of the action callback, for admin purposes. */
    descriptor: IDescriptor,

    /** The action callback. */
    callback: RuleCallback,
}> = new Map();
export default actions;

/**
 * A helper function to import all files within a directory, and use the filename as the action ID.
 *
 * @param dir The directory to search in.
 */
export async function ImportActions(dir: string = join(__dirname, "..", "Actions")): Promise<void> {
    (await glob(join(dir, "**", "*.+(js|ts)"), {
        ignore: [ "**/*.d.ts" ],
    })).forEach((file) => actions.set(basename(file).slice(0, -3), {
        callback: require(file).default,
        descriptor: require(file).descriptor,
    }));
}
