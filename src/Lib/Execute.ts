/**
 * Provides the rules execution callback.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import Sort from "@depax/sort";
import actions from "./Actions";
import conditions from "./Conditions";
import IRule, {
    EGroupOp,
    EResponse,
    IActionReport,
    ICondition,
    IConditionGroup,
    IConditionReport,
    IObject,
    IReport,
} from "./Interfaces";

/**
 * Execute the given conditions collection, this takes into account if the conditions is a group or not.
 *
 * @param args The arguments that had been passed to the execute callback.
 * @param items An array of conditions and.or condition groups.
 * @param reports A reference to the condition reports collection.
 * @param op The condition passing operation, "And" or "Or".
 *
 * @return Returns the success state of the condition.
 */
async function ExecuteConditions(
    args: IObject,
    items: Array<ICondition | IConditionGroup>,
    reports: IConditionReport[],
    op: EGroupOp = EGroupOp.And,
): Promise<boolean> {
    let success: boolean = true;
    let atLeastOneSuccess: boolean = false;

    Sort(items, "weight");
    for (const details of items) {
        if ((details as any).conditions) {
            const conditionReport: IConditionReport = {
                args: Object.assign({}, args),
                group: {
                    conditions: [],
                    op: (details as IConditionGroup).op,
                    weight: 0,
                },
                response: EResponse.Pass,
            };

            if ((details as IConditionGroup).weight !== undefined) {
                conditionReport.group.weight = (details as IConditionGroup).weight;
            }

            reports.push(conditionReport);

            if (
                !await ExecuteConditions(
                    args,
                    (details as IConditionGroup).conditions,
                    conditionReport.group.conditions,
                    (details as IConditionGroup).op,
                ) &&
                !(details as IConditionGroup).negate
            ) {
                conditionReport.response = EResponse.Fail;
                success = false;
            }
        } else {
            const conditionReport: IConditionReport = {
                args: Object.assign({}, args),
                condition: details as ICondition,
                response: EResponse.Skip,
            };

            reports.push(conditionReport);

            const condition = conditions.get((details as ICondition).id);
            if (!condition) {
                conditionReport.error = `The condition type "${(details as ICondition).id}" is unknown.`;
                continue;
            }

            try {
                conditionReport.response = await condition.callback(args, (details as ICondition).config);
            } catch (err) {
                conditionReport.error = err.message;
                conditionReport.response = EResponse.Errored;
            }

            if (details.negate && [ EResponse.Pass, EResponse.Fail ].indexOf(conditionReport.response) >= 0) {
                conditionReport.response = conditionReport.response === EResponse.Pass ?
                    EResponse.Fail :
                    EResponse.Pass;
            }

            if ([ EResponse.Fail, EResponse.Errored ].indexOf(conditionReport.response) >= 0) {
                success = false;
            } else {
                atLeastOneSuccess = true;
            }
        }
    }

    return op === EGroupOp.And ? success : atLeastOneSuccess;
}

/**
 * Execute the given rule object.
 *
 * @param rule The rule object to execute.
 * @param args Any arguments to pass onto the the rule conditions and actions.
 *
 * @return Returns the generated report.
 */
export default async function Execute(rule: IRule, args: IObject): Promise<IReport> {
    // Create a copy of the args, for reference as the original args.
    const originalArgs: IObject = Object.assign({}, args);

    // Create the initial report object.
    const report: IReport = {
        actions: [],
        args,
        conditions: [],
        originalArgs,
        rule,
        success: true,
    };

    // Execute all the conditions, and update the report success value.
    report.success = await ExecuteConditions(args, rule.conditions || [], report.conditions);

    // Check we successfully executed the conditions.
    if (report.success) {
        // Sort the actions collection.
        Sort(rule.actions, "weight");

        // Cycle through the defined actions.
        for (const details of rule.actions) {
            // Generate the action report object, and add it to the report object.
            const actionReport: IActionReport = {
                action: details,
                args: Object.assign({}, args),
                response: EResponse.Skip,
            };

            report.actions.push(actionReport);

            // If the report has failed, i.e. a previous action failed, then we dont want to continue.
            if (!report.success) {
                actionReport.error = "Earlier action had failed.";
                continue;
            }

            // Get the action callback, and check we actually have one, if not we want to skip this action.
            const action = actions.get(details.id);
            if (!action) {
                actionReport.error = `The action type "${details.id}" is unknown.`;
                continue;
            }

            // Execute the action and store the response, and if the response is a fail then mark the report as failed.
            try {
                actionReport.response = await action.callback(args, details.config);
            } catch (err) {
                actionReport.error = err.message;
                actionReport.response = EResponse.Errored;
            }

            if ([ EResponse.Fail, EResponse.Errored ].indexOf(actionReport.response) >= 0) {
                report.success = false;
            }
        }
    }

    return report;
}
