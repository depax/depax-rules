/**
 * Provides the various types for the condition callbacks.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import { basename, join } from "path";
import { RuleCallback } from "../Rules";
import glob from "../Utils/Glob";
import { IDescriptor } from "./Interfaces";

/** A collection of the defined condition callbacks. */
const conditions: Map<string, {
    /** A descriptor of the condition callback, for admin purposes. */
    descriptor: IDescriptor,

    /** The condition callback. */
    callback: RuleCallback,
}> = new Map();
export default conditions;

/**
 * A helper function to import all files within a directory, and use the filename as the condition ID.
 *
 * @param dir The directory to search in.
 */
export async function ImportConditions(dir: string = join(__dirname, "..", "Conditions")): Promise<void> {
    (await glob(join(dir, "**", "*.+(js|ts)"), {
        ignore: [ "**/*.d.ts" ],
    })).forEach((file) => conditions.set(basename(file).slice(0, -3), {
        callback: require(file).default,
        descriptor: require(file).descriptor,
    }));
}
