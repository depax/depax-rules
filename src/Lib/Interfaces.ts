/**
 * Provides all the interface definitions.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

/** The response type used in Conditions and Actions. */
export enum EResponse {
    /** The condition/action had successfully executed. */
    Pass = "pass",

    /** The condition/action had failed its execution. */
    Fail = "fail",

    /** The condition/action had skipped its execution. */
    Skip = "skip",

    /** The condition/action had errored. */
    Errored = "error",
}

/** A generic object interface. */
export interface IObject {
    [name: string]: any;
}

/** The config value types supported. */
export enum EConfigDescriptorType {
    /** A string type. */
    String = "string",

    /** A boolean type. */
    Boolean = "boolean",

    /** A number type. */
    Number = "number",

    /** An object or array type. */
    Json = "json",

    /** Any type. */
    Any = "any",
}

/** An interface to describe the Action/Condition callback. */
export interface IDescriptor {
    /** The description of the callback. */
    description: string;

    /** A description of the callbacks intended config. */
    config?: {
        /** Each config value and its descriptor. */
        [name: string]: {
            /** A description of the config value. */
            description: string;

            /** Is this config value required. */
            required: boolean;

            /** The value type of the config. */
            type: EConfigDescriptorType;

            /** An optional list of available options. */
            values?: { [key: string]: string };
        };
    };
}

/** The definition of the rule object. */
export default interface IRule {
    /** An admin title of the rule. */
    title?: string;

    /** An admin description of the rule. */
    description?: string;

    /** An admin group of tags to group the rule. */
    tags?: string[];

    /** A collection of defined conditions and/or condition groups. */
    conditions?: Array<ICondition | IConditionGroup>;

    /** A collection of defined actions. */
    actions: IAction[];
}

/** The event descriptor. */
export interface IEvent {
    /** The admin title of the event. */
    title: string;

    /** The admin description of the event. */
    description?: string;

    /** A category for the event for grouping. */
    category?: string;
}

/** An interface to describe the rule condition. */
export interface ICondition {
    /** The Type ID of the condition to execute. */
    id: string;

    /** The config object to pass to the condition callback. */
    config: object;

    /** Determine if the response of the callback should be negated. */
    negate?: boolean;

    /** The weight of the condition. */
    weight?: number;
}

/** The condition group operator. */
export enum EGroupOp {
    /** All conditions in the group must pass. */
    And = "and",

    /** At least one condition in the group must pass. */
    Or = "or",
}

/** A group of multiple conditions. */
export interface IConditionGroup {
    /** The operation of the group. */
    op: EGroupOp;

    /** A collection of conditions and/or condition groups. */
    conditions: Array<ICondition | IConditionGroup>;

    /** Determine if the response of the condition group should be negated. */
    negate?: boolean;

    /** The weight of the condition group. */
    weight?: number;
}

/* An interface to describe the rule action. */
export interface IAction {
    /** The Type ID of the action to execute. */
    id: string;

    /** The config to pass to the action callback. */
    config: object;

    /** The weight of the action. */
    weight?: number;
}

/** An interface to describe the event execute report. */
export interface IEventReport {
    /** The event that had been executed. */
    event: string;

    /** The group that had been executed. */
    group?: string;

    /** The reports of each rule executed under the event and group. */
    reports: Array<{
        /** The ID of the rule that had been executed. */
        ruleId: string;

        /** The rules execution report. */
        report: IReport;
    }>;
}

/** An interface to describe the rule report. */
export interface IReport {
    /** A reference to the rule object that had been executed. */
    rule: IRule;

    /** The original args sent to the execution without any modifications from the conditions and/or actions. */
    originalArgs: IObject;

    /** The args sent to the execution, this may get altered by the conditions and/or actions. */
    args: IObject;

    /** A collection of the condition reports. */
    conditions: IConditionReport[];

    /** A collection of the action reports. */
    actions: any[];

    /** The overall success of the rule. */
    success: boolean;
}

/** An interface to describe the condition report. */
export interface IConditionReport {
    /** The arguments sent to the condition callback. */
    args: IObject;

    /** A reference to the condition details, if this was from a condition. */
    condition?: ICondition;

    /** Details of the condition group, if this was from a condition group. */
    group?: {
        /** The operator of the condition group. */
        op: EGroupOp;

        /** A collection of the condition reports. */
        conditions: IConditionReport[];

        /** The weight of the condition group. */
        weight: number;
    };

    /** The error message if the condition had errored. */
    error?: string;

    /** The response state of the condition callback. */
    response: EResponse;
}

/** An interface to describe the action report. */
export interface IActionReport {
    /** The arguments sent to the action callback. */
    args: IObject;

    /** A reference to the action details. */
    action: IAction;

    /** The error message if the condition had errored. */
    error?: string;

    /** The response state of the condition callback. */
    response: EResponse;
}
