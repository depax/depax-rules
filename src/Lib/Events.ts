/**
 * Provides the various types for the event details.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import Sort from "@depax/sort";
import { IEvent, IEventReport, IObject, IReport } from "../Lib/Interfaces";
import rules from "../Rules";
import Execute from "./Execute";

/** A collection of the defined event details. */
const events: Map<string, IEvent> = new Map();
export default events;

/** A collection of the defined event and group mappings. */
export const mappings: Map<string, Map<string, Array<{
    /** The Rule ID to execute. */
    ruleId: string,

    /** A weight to apply when the event and group is being called. */
    weight: number,
}>>> = new Map();

/**
 * Define a mapped rule.
 *
 * @param ruleId The ID of the rule to execute, the rule must have been registered in the `rules` map first.
 * @param event The event to bind this rule to.
 * @param weight A weight to apply to thie binding.
 * @param group An optional group to bind this rule to.
 */
export function MapRule(ruleId: string, event: string, weight: number = 0, group: string = "_"): void {
    // Check if the event has already been mapped, if not then create the internal map collection.
    if (mappings.has(event) === false) {
        mappings.set(event, new Map());
    }

    // Get the groups, and check if the group has been defined, if not then create it.
    const groups = mappings.get(event);
    if (groups.has(group) === false) {
        groups.set(group, []);
    }

    // Get the Rule IDs details array, and push the new details in to and update the collection.
    const ruleIds = groups.get(group);
    ruleIds.push({ ruleId, weight });
    groups.set(group, ruleIds);
}

/**
 * Executes a given event and/or group.
 *
 * @param event The event to fire.
 * @param args The arguments to apply to each executed rule.
 * @param group An optional group, if not supplied then all rules within the event are fired.
 *
 * @return Returns an event report containing details of the fired rules and their reports.
 */
export async function ExecuteEvent(event: string, args: IObject, group?: string): Promise<IEventReport> {
    // Generate the event report object.
    const reports: IEventReport = {
        event,
        group: group || null,
        reports: [],
    };

    // Check we have this event mapped.
    if (mappings.has(event)) {
        // Get the groups from the mappings.
        const groups = mappings.get(event);

        // If a group has been defined, then run just that group, or we get all the group names and run them all.
        const groupNames = group ? [ group ] : Array.from(groups.keys());

        for (const name of groupNames) {
            // Get the Rule ID collection, and check we actually get something.
            const ruleIds = groups.get(name);
            /* istanbul ignore next */
            if (!ruleIds) {
                continue;
            }

            // Sort the Rule ID details.
            Sort(ruleIds, "weight");

            // Cycle through the defined Rule IDs.
            for (const item of ruleIds) {
                // Attempt to get the rule from the rules collection, if not then we skip.
                const rule = rules.get(item.ruleId);
                /* istanbul ignore next */
                if (!rule) {
                    continue;
                }

                // We don't want the arguments changing after each rule, so make a copy of the args.
                const ruleArgs = Object.assign({}, args);

                // Execute the rule and add the report details to the event report.
                reports.reports.push({
                    report: await Execute(rule, ruleArgs),
                    ruleId: item.ruleId,
                });
            }
        }
    }

    return reports;
}
