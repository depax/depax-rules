/**
 * Provides the packages main file, referencing all the relevant items.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import actions, { ImportActions } from "./Lib/Actions";
import conditions, { ImportConditions } from "./Lib/Conditions";
import events, { ExecuteEvent, mappings, MapRule } from "./Lib/Events";
import Execute from "./Lib/Execute";
import IRule, {
    EConfigDescriptorType,
    EGroupOp,
    EResponse,
    IAction,
    ICondition,
    IConditionGroup,
    IDescriptor,
    IEvent,
    IEventReport,
    IObject,
    IReport,
} from "./Lib/Interfaces";
import rules, { RuleCallback } from "./Rules";
import ParseArgToken from "./Utils/ParseArgToken";

export default Execute;
export {
    IObject, IDescriptor, IEventReport, IReport, IRule, IAction, ICondition, IConditionGroup, IEvent,
    EConfigDescriptorType, EResponse, EGroupOp,
    RuleCallback,
    rules,
    actions, ImportActions,
    conditions, ImportConditions,
    events, mappings,
    MapRule,
    ExecuteEvent,
    ParseArgToken,
};
