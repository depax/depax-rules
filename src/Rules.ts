/**
 * Contains the rules collection and callback definition.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import IRule, { EResponse, IObject } from "./Lib/Interfaces";

/** A collection of defined rule objects, used in mappings or just storage. */
const rules: Map<string, IRule> = new Map();
export default rules;

/** The callback definition for Conditions and Actions. */
export type RuleCallback = (args: IObject, config: IObject) => Promise<EResponse>;
