/**
 * Provides the "IsEqual" condition callback.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import { deepEqual, equal } from "assert";
import { EConfigDescriptorType, EResponse, IDescriptor, IObject } from "../Lib/Interfaces";
import ParseArgToken from "../Utils/ParseArgToken";

export const descriptor: IDescriptor = {
    config: {
        value1: {
            description: "The value we want to compare with.",
            required: true,
            type: EConfigDescriptorType.Any,
        },
        value2: {
            description: "The value that we are comparing to.",
            required: true,
            type: EConfigDescriptorType.Any,
        },
    },
    description: "Determines if two values match each other.",
};

export default async function IsEqual(args: IObject, config: IObject): Promise<EResponse> {
    // Make sure the config has the required properties provided.
    if (!(!!config.actual && !!config.expected)) {
        throw new Error("Missing the 'actual' and/or 'expected' properties in the config.");
    }

    const actual = ParseArgToken(config.actual, args);
    const expected = ParseArgToken(config.expected, args);

    try {
        if (actual instanceof Object) {
            deepEqual(actual, expected);
        } else {
            equal(actual, expected);
        }

        return EResponse.Pass;
    } catch (err) {
        return EResponse.Fail;
    }
}
