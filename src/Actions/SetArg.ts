/**
 * Provides the "SetArg" action callback.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import { EConfigDescriptorType, EResponse, IDescriptor, IObject } from "../Lib/Interfaces";
import ParseArgToken from "../Utils/ParseArgToken";

export const descriptor: IDescriptor = {
    config: {
        arg: {
            description: "The name of the argument to add or update.",
            required: true,
            type: EConfigDescriptorType.String,
        },
        value: {
            description: "The value to apply to the argument.",
            required: true,
            type: EConfigDescriptorType.Any,
        },
    },
    description: "Set an argument value.",
};

export default async function SetArg(args: IObject, config: IObject): Promise<EResponse> {
    // Make sure the config has the required properties provided.
    if (!(!!config.arg && !!config.value)) {
        throw new Error("Missing the 'arg' and/or 'value' properties in the config.");
    }

    const arg = ParseArgToken(config.arg, args);
    const value = ParseArgToken(config.value, args);

    args[arg] = value;
    return EResponse.Pass;
}
