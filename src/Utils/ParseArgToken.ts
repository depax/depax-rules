/**
 * Provides a utility function to parse tokens from args provided to the rules.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import Data from "@depax/data";
import { IObject } from "../Lib/Interfaces";

/**
 * A helper function to translate an arg token "@arg1" into the argument name, if applicable.
 *
 * @param value The value holding the potential argument token.
 * @param args The arguments object.
 *
 * @return Returns the argument value if it's a token, otherwise the value is returned as was given.
 */
export default function ParseArgToken(value: any, args: IObject): any {
    if (typeof value !== "string") {
        return value;
    }

    const data = new Data(args);
    return value[0] !== "@" ? value : data.get(value.substring(1));
}
