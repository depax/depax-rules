/**
 * A utility function to promisify the glob function.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import { promisify } from "util";

/* tslint:disable-next-line:no-var-requires */
const glob = promisify(require("glob"));
export default glob;
