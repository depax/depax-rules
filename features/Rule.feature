@rules @rule
Feature: Should be able to create and execute a rule

  Scenario: I should be able to create a rule and will fail due to an error
    Given I define the available conditions
      And I define the available actions
     When I create the rule:
      """
      {
        "actions": [{ "id": "SetArg", "config": { "arg": "message", "value": "hello" } }],
        "conditions": [{ "id": "IsEqual", "config": {} }]
      }
      """
      And I execute the rule with the following arguments:
      """
      { "arg1": false }
      """
     Then The executed rule should have failed
      And The report should be:
      """
      {
        "args": { "arg1": false },
        "originalArgs": { "arg1": false },
        "conditions": [{
          "args": { "arg1": false },
          "condition": {
            "config": {},
            "id": "IsEqual"
          },
          "error": "Missing the 'actual' and/or 'expected' properties in the config.",
          "response": "error"
        }],
        "actions": [],
        "rule": {
          "conditions": [{
            "config": {},
            "id": "IsEqual"
          }],
          "actions": [{
            "config": { "arg": "message", "value": "hello" },
            "id": "SetArg"
          }]
        },
        "success": false
      }
      """

  Scenario: I should be able to create a rule and will fail due to a condition
    Given I define the available conditions
      And I define the available actions
     When I create the rule:
      """
      {
        "actions": [{ "id": "SetArg", "config": { "arg": "message", "value": "hello" } }],
        "conditions": [{ "id": "IsEqual", "config": { "actual": "@arg1", "expected": true } }]
      }
      """
      And I execute the rule with the following arguments:
      """
      { "arg1": false }
      """
     Then The executed rule should have failed
      And The report should be:
      """
      {
        "args": { "arg1": false },
        "originalArgs": { "arg1": false },
        "conditions": [{
          "args": { "arg1": false },
          "condition": {
            "config": { "actual": "@arg1", "expected": true },
            "id": "IsEqual"
          },
          "response": "fail"
        }],
        "actions": [],
        "rule": {
          "conditions": [{
            "config": { "actual": "@arg1", "expected": true },
            "id": "IsEqual"
          }],
          "actions": [{
            "config": { "arg": "message", "value": "hello" },
            "id": "SetArg"
          }]
        },
        "success": false
      }
      """

  Scenario: I should be able to create a rule and will pass and execute the action
    Given I define the available conditions
      And I define the available actions
     When I create the rule:
      """
      {
        "actions": [{ "id": "SetArg", "config": { "arg": "message", "value": "hello" } }],
        "conditions": [{ "id": "IsEqual", "config": { "actual": "@arg1", "expected": true } }]
      }
      """
      And I execute the rule with the following arguments:
      """
      { "arg1": true }
      """
     Then The executed rule should have passed
      And The report should be:
      """
      {
        "args": { "arg1": true, "message": "hello" },
        "originalArgs": { "arg1": true },
        "conditions": [{
          "args": { "arg1": true },
          "condition": {
            "config": { "actual": "@arg1", "expected": true },
            "id": "IsEqual"
          },
          "response": "pass"
        }],
        "actions": [{
          "action": { "config": { "arg": "message", "value": "hello" }, "id": "SetArg" },
          "args": { "arg1": true },
          "response": "pass"
        }],
        "rule": {
          "conditions": [{
            "config": { "actual": "@arg1", "expected": true },
            "id": "IsEqual"
          }],
          "actions": [{
            "config": { "arg": "message", "value": "hello" },
            "id": "SetArg"
          }]
        },
        "success": true
      }
      """
      And The arguments should be:
      """
      { "arg1": true, "message": "hello" }
      """

  Scenario: I should be able to create a rule and will fail with a negated condition
    Given I define the available conditions
      And I define the available actions
     When I create the rule:
      """
      {
        "actions": [{ "id": "SetArg", "config": { "arg": "message", "value": "hello" } }],
        "conditions": [{ "id": "IsEqual", "config": { "actual": "@arg1", "expected": true }, "negate": true }]
      }
      """
      And I execute the rule with the following arguments:
      """
      { "arg1": true }
      """
     Then The executed rule should have failed
      And The report should be:
      """
      {
        "args": { "arg1": true },
        "originalArgs": { "arg1": true },
        "conditions": [{
          "args": { "arg1": true },
          "condition": {
            "config": { "actual": "@arg1", "expected": true },
            "id": "IsEqual",
            "negate": true
          },
          "response": "fail"
        }],
        "actions": [],
        "rule": {
          "conditions": [{
            "config": { "actual": "@arg1", "expected": true },
            "id": "IsEqual",
            "negate": true
          }],
          "actions": [{
            "config": { "arg": "message", "value": "hello" },
            "id": "SetArg"
          }]
        },
        "success": false
      }
      """
      And The arguments should be:
      """
      { "arg1": true }
      """

  Scenario: I should be able to create a rule and will fail due to a condition group failure
    Given I define the available conditions
      And I define the available actions
     When I create the rule:
      """
      {
        "actions": [{ "id": "SetArg", "config": { "arg": "message", "value": "hello" } }],
        "conditions": [{
          "op": "and",
          "conditions": [
            { "id": "IsEqual", "config": { "actual": "@arg1", "expected": true } },
            { "id": "IsEqual", "config": { "actual": "@arg2", "expected": true } }
          ]
        }]
      }
      """
      And I execute the rule with the following arguments:
      """
      { "arg1": true, "arg2": false }
      """
     Then The executed rule should have failed
      And The report should be:
      """
      {
        "args": { "arg1": true, "arg2": false },
        "originalArgs": { "arg1": true, "arg2": false },
        "conditions": [{
          "args": { "arg1": true, "arg2": false },
          "group": {
            "op": "and",
            "weight": 0,
            "conditions": [{
              "args": { "arg1": true, "arg2": false },
              "condition": {
                "config": { "actual": "@arg1", "expected": true },
                "id": "IsEqual"
              },
              "response": "pass"
            }, {
              "args": { "arg1": true, "arg2": false },
              "condition": {
                "config": { "actual": "@arg2", "expected": true },
                "id": "IsEqual"
              },
              "response": "fail"
            }]
          },
          "response": "fail"
        }],
        "actions": [],
        "rule": {
          "conditions": [{
            "op": "and",
            "conditions": [{
              "config": { "actual": "@arg1", "expected": true },
              "id": "IsEqual"
            }, {
              "config": { "actual": "@arg2", "expected": true },
              "id": "IsEqual"
            }]
          }],
          "actions": [{
            "config": { "arg": "message", "value": "hello" },
            "id": "SetArg"
          }]
        },
        "success": false
      }
      """

  Scenario: I should be able to create a rule and will pass due to a condition group and an or operator
    Given I define the available conditions
      And I define the available actions
     When I create the rule:
      """
      {
        "actions": [{ "id": "SetArg", "config": { "arg": "message", "value": "hello" } }],
        "conditions": [{
          "op": "or",
          "conditions": [
            { "id": "IsEqual", "config": { "actual": "@arg1", "expected": true } },
            { "id": "IsEqual", "config": { "actual": "@arg2", "expected": true } }
          ]
        }]
      }
      """
      And I execute the rule with the following arguments:
      """
      { "arg1": true, "arg2": false }
      """
     Then The executed rule should have passed
      And The report should be:
      """
      {
        "args": { "arg1": true, "arg2": false, "message": "hello" },
        "originalArgs": { "arg1": true, "arg2": false },
        "conditions": [{
          "args": { "arg1": true, "arg2": false },
          "group": {
            "op": "or",
            "weight": 0,
            "conditions": [{
              "args": { "arg1": true, "arg2": false },
              "condition": {
                "config": { "actual": "@arg1", "expected": true },
                "id": "IsEqual"
              },
              "response": "pass"
            }, {
              "args": { "arg1": true, "arg2": false },
              "condition": {
                "config": { "actual": "@arg2", "expected": true },
                "id": "IsEqual"
              },
              "response": "fail"
            }]
          },
          "response": "pass"
        }],
        "actions": [{
          "action": { "config": { "arg": "message", "value": "hello" }, "id": "SetArg" },
          "args": { "arg1": true, "arg2": false },
          "response": "pass"
        }],
        "rule": {
          "conditions": [{
            "op": "or",
            "conditions": [{
              "config": { "actual": "@arg1", "expected": true },
              "id": "IsEqual"
            }, {
              "config": { "actual": "@arg2", "expected": true },
              "id": "IsEqual"
            }]
          }],
          "actions": [{
            "config": { "arg": "message", "value": "hello" },
            "id": "SetArg"
          }]
        },
        "success": true
      }
      """

  Scenario: I should be able to create a rule and will pass all conditions
    Given I define the available conditions
      And I define the available actions
     When I create the rule:
      """
      {
        "actions": [{ "id": "SetArg", "config": { "arg": "message", "value": "hello" } }],
        "conditions": [{
          "op": "and",
          "conditions": [
            { "id": "IsEqual", "config": { "actual": "@arg1", "expected": true } },
            { "id": "IsEqual", "config": { "actual": "@options.arg2", "expected": true } }
          ]
        }]
      }
      """
      And I execute the rule with the following arguments:
      """
      { "arg1": true, "options": { "arg2": true } }
      """
     Then The executed rule should have passed
      And The report should be:
      """
      {
        "args": { "arg1": true, "options": { "arg2": true }, "message": "hello" },
        "originalArgs": { "arg1": true, "options": { "arg2": true } },
        "conditions": [{
          "args": { "arg1": true, "options": { "arg2": true } },
          "group": {
            "op": "and",
            "weight": 0,
            "conditions": [{
              "args": { "arg1": true, "options": { "arg2": true } },
              "condition": {
                "config": { "actual": "@arg1", "expected": true },
                "id": "IsEqual"
              },
              "response": "pass"
            }, {
              "args": { "arg1": true, "options": { "arg2": true } },
              "condition": {
                "config": { "actual": "@options.arg2", "expected": true },
                "id": "IsEqual"
              },
              "response": "pass"
            }]
          },
          "response": "pass"
        }],
        "actions": [{
          "action": { "config": { "arg": "message", "value": "hello" }, "id": "SetArg" },
          "args": { "arg1": true, "options": { "arg2": true } },
          "response": "pass"
        }],
        "rule": {
          "conditions": [{
            "op": "and",
            "conditions": [{
              "config": { "actual": "@arg1", "expected": true },
              "id": "IsEqual"
            }, {
              "config": { "actual": "@options.arg2", "expected": true },
              "id": "IsEqual"
            }]
          }],
          "actions": [{
            "config": { "arg": "message", "value": "hello" },
            "id": "SetArg"
          }]
        },
        "success": true
      }
      """

  Scenario: The conditions and actions should be sorted by weight
    Given I define the available conditions
      And I define the available actions
     When I create the rule:
      """
      {
        "actions": [
          { "id": "SetArg", "config": { "arg": "message1", "value": "hello" }, "weight": 25 },
          { "id": "SetArg", "config": { "arg": "message2", "value": "hello" } },
          { "id": "SetArg", "config": { "arg": "message3", "value": "hello" }, "weight": -10 }
        ],
        "conditions": [
          { "id": "IsEqual", "config": { "actual": "@arg1", "expected": true }, "weight": 25 },
          { "id": "IsEqual", "config": { "actual": "@arg2", "expected": true } },
          { "op": "or", "conditions": [
            { "id": "IsEqual", "config": { "actual": "@arg1", "expected": true }, "weight": 25 },
            { "id": "IsEqual", "config": { "actual": "@arg2", "expected": true } },
            { "id": "IsEqual", "config": { "actual": "@arg3", "expected": true }, "weight": -10 }
          ], "weight": 5 },
          { "id": "IsEqual", "config": { "actual": "@arg3", "expected": true }, "weight": -10 }
        ]
      }
      """
      And I execute the rule with the following arguments:
      """
      { "arg1": true, "arg2": true, "arg3": true }
      """
     Then The executed rule should have passed
      And The report should be:
      """
      {
        "args": { "arg1": true, "arg2": true, "arg3": true, "message1": "hello", "message2": "hello", "message3": "hello" },
        "originalArgs": { "arg1": true, "arg2": true, "arg3": true },
        "conditions": [{
          "args": { "arg1": true, "arg2": true, "arg3": true },
          "condition": {
            "config": { "actual": "@arg3", "expected": true },
            "id": "IsEqual",
            "weight": -10
          },
          "response": "pass"
        }, {
          "args": { "arg1": true, "arg2": true, "arg3": true },
          "condition": {
            "config": { "actual": "@arg2", "expected": true },
            "id": "IsEqual"
          },
          "response": "pass"
        }, {
          "args": { "arg1": true, "arg2": true, "arg3": true },
          "group": {
            "op": "or",
            "conditions": [{
              "args": { "arg1": true, "arg2": true, "arg3": true },
              "condition": {
                "config": { "actual": "@arg3", "expected": true },
                "id": "IsEqual",
                "weight": -10
              },
              "response": "pass"
            }, {
              "args": { "arg1": true, "arg2": true, "arg3": true },
              "condition": {
                "config": { "actual": "@arg2", "expected": true },
                "id": "IsEqual"
              },
              "response": "pass"
            }, {
              "args": { "arg1": true, "arg2": true, "arg3": true },
              "condition": {
                "config": { "actual": "@arg1", "expected": true },
                "id": "IsEqual",
                "weight": 25
              },
              "response": "pass"
            }],
            "weight": 5
          },
          "response": "pass"
        }, {
          "args": { "arg1": true, "arg2": true, "arg3": true },
          "condition": {
            "config": { "actual": "@arg1", "expected": true },
            "id": "IsEqual",
            "weight": 25
          },
          "response": "pass"
        }],
        "actions": [{
          "action": { "config": { "arg": "message3", "value": "hello" }, "id": "SetArg", "weight": -10 },
          "args": { "arg1": true, "arg2": true, "arg3": true },
          "response": "pass"
        }, {
          "action": { "config": { "arg": "message2", "value": "hello" }, "id": "SetArg" },
          "args": { "arg1": true, "arg2": true, "arg3": true, "message3": "hello" },
          "response": "pass"
        }, {
          "action": { "config": { "arg": "message1", "value": "hello" }, "id": "SetArg", "weight": 25 },
          "args": { "arg1": true, "arg2": true, "arg3": true, "message2": "hello", "message3": "hello" },
          "response": "pass"
        }],
        "rule": {
          "conditions": [{
            "config": { "actual": "@arg3", "expected": true },
            "id": "IsEqual",
            "weight": -10
          }, {
            "config": { "actual": "@arg2", "expected": true },
            "id": "IsEqual"
          }, {
            "op": "or",
            "conditions": [
              { "id": "IsEqual", "config": { "actual": "@arg3", "expected": true }, "weight": -10 },
              { "id": "IsEqual", "config": { "actual": "@arg2", "expected": true } },
              { "id": "IsEqual", "config": { "actual": "@arg1", "expected": true }, "weight": 25 }
            ],
            "weight": 5
          }, {
            "config": { "actual": "@arg1", "expected": true },
            "id": "IsEqual",
            "weight": 25
          }],
          "actions": [{
            "config": { "arg": "message3", "value": "hello" },
            "id": "SetArg",
            "weight": -10
          }, {
            "config": { "arg": "message2", "value": "hello" },
            "id": "SetArg"
          }, {
            "config": { "arg": "message1", "value": "hello" },
            "id": "SetArg",
            "weight": 25
          }]
        },
        "success": true
      }
      """

  Scenario: Should bypass unknown conditions and actions
    Given I define the available conditions
      And I define the available actions
     When I create the rule:
      """
      {
        "actions": [
          { "id": "SetArg", "config": { "arg": "message", "value": "hello" } },
          { "id": "UnknownAction", "config": {} }
        ],
        "conditions": [{
          "op": "and",
          "conditions": [
            { "id": "IsEqual", "config": { "actual": "@arg1", "expected": true } },
            { "id": "UnknownCondition", "config": {} },
            { "id": "IsEqual", "config": { "actual": "@arg2", "expected": true } }
          ]
        }]
      }
      """
      And I execute the rule with the following arguments:
      """
      { "arg1": true, "arg2": true }
      """
     Then The executed rule should have passed
      And The report should be:
      """
      {
        "args": { "arg1": true, "arg2": true, "message": "hello" },
        "originalArgs": { "arg1": true, "arg2": true },
        "conditions": [{
          "args": { "arg1": true, "arg2": true },
          "group": {
            "op": "and",
            "weight": 0,
            "conditions": [{
              "args": { "arg1": true, "arg2": true },
              "condition": { "config": { "actual": "@arg1", "expected": true }, "id": "IsEqual" },
              "response": "pass"
            }, {
              "args": { "arg1": true, "arg2": true },
              "condition": { "config": {}, "id": "UnknownCondition" },
              "error": "The condition type \"UnknownCondition\" is unknown.",
              "response": "skip"
            }, {
              "args": { "arg1": true, "arg2": true },
              "condition": { "config": { "actual": "@arg2", "expected": true }, "id": "IsEqual" },
              "response": "pass"
            }]
          },
          "response": "pass"
        }],
        "actions": [{
          "action": { "config": { "arg": "message", "value": "hello" }, "id": "SetArg" },
          "args": { "arg1": true, "arg2": true },
          "response": "pass"
        }, {
          "action": { "config": {}, "id": "UnknownAction" },
          "args": { "arg1": true, "arg2": true, "message": "hello" },
          "error": "The action type \"UnknownAction\" is unknown.",
          "response": "skip"
        }],
        "rule": {
          "conditions": [{
            "op": "and",
            "conditions": [
              { "config": { "actual": "@arg1", "expected": true }, "id": "IsEqual" },
              { "config": {}, "id": "UnknownCondition" },
              { "config": { "actual": "@arg2", "expected": true }, "id": "IsEqual" }
            ]
          }],
          "actions": [
            { "config": { "arg": "message", "value": "hello" }, "id": "SetArg" },
            { "config": {}, "id": "UnknownAction" }
          ]
        },
        "success": true
      }
      """

  Scenario: Should not run actions after a failing action
    Given I define the available conditions
      And I define the available actions
      And I register the failing action
     When I create the rule:
      """
      {
        "actions": [
          { "id": "SetArg", "config": { "arg": "message", "value": "hello" } },
          { "id": "FailingAction", "config": {} },
          { "id": "SetArg", "config": { "arg": "message", "value": "foo" } }
        ],
        "conditions": [{
          "op": "and",
          "conditions": [
            { "id": "IsEqual", "config": { "actual": "@arg1", "expected": true } },
            { "id": "IsEqual", "config": { "actual": "@arg2", "expected": true } }
          ]
        }]
      }
      """
      And I execute the rule with the following arguments:
      """
      { "arg1": true, "arg2": true }
      """
     Then The executed rule should have failed
      And The report should be:
      """
      {
        "args": { "arg1": true, "arg2": true, "message": "hello" },
        "originalArgs": { "arg1": true, "arg2": true },
        "conditions": [{
          "args": { "arg1": true, "arg2": true },
          "group": {
            "op": "and",
            "weight": 0,
            "conditions": [{
              "args": { "arg1": true, "arg2": true },
              "condition": { "config": { "actual": "@arg1", "expected": true }, "id": "IsEqual" },
              "response": "pass"
            }, {
              "args": { "arg1": true, "arg2": true },
              "condition": { "config": { "actual": "@arg2", "expected": true }, "id": "IsEqual" },
              "response": "pass"
            }]
          },
          "response": "pass"
        }],
        "actions": [{
          "action": { "config": { "arg": "message", "value": "hello" }, "id": "SetArg" },
          "args": { "arg1": true, "arg2": true },
          "response": "pass"
        }, {
          "action": { "config": {}, "id": "FailingAction" },
          "args": { "arg1": true, "arg2": true, "message": "hello" },
          "response": "fail"
        }, {
          "action": { "config": { "arg": "message", "value": "foo" }, "id": "SetArg" },
          "args": { "arg1": true, "arg2": true, "message": "hello" },
          "error": "Earlier action had failed.",
          "response": "skip"
        }],
        "rule": {
          "conditions": [{
            "op": "and",
            "conditions": [
              { "config": { "actual": "@arg1", "expected": true }, "id": "IsEqual" },
              { "config": { "actual": "@arg2", "expected": true }, "id": "IsEqual" }
            ]
          }],
          "actions": [
            { "config": { "arg": "message", "value": "hello" }, "id": "SetArg" },
            { "config": {}, "id": "FailingAction" },
            { "config": { "arg": "message", "value": "foo" }, "id": "SetArg" }
          ]
        },
        "success": false
      }
      """

  Scenario: Should map the rule and execute it via event
    Given I define the available conditions
      And I define the available actions
      And I register the failing action
     When I create the rule:
      """
      {
        "actions": [
          { "id": "SetArg", "config": { "arg": "message", "value": "hello" } }
        ],
        "conditions": [
          { "id": "IsEqual", "config": { "actual": "@arg1", "expected": true } }
        ]
      }
      """
      And I map the rule with ID "test" to event "my-event"
      And I create the rule:
      """
      {
        "actions": [
          { "id": "SetArg", "config": { "arg": "message", "value": "world" } }
        ],
        "conditions": [
          { "id": "IsEqual", "config": { "actual": "@arg1", "expected": true } }
        ]
      }
      """
      And I map the rule with ID "test2" to event "my-event" with weight -5
      And I create the rule:
      """
      {
        "actions": [
          { "id": "SetArg", "config": { "arg": "message", "value": "foo" } }
        ],
        "conditions": [
          { "id": "IsEqual", "config": { "actual": "@arg1", "expected": true } }
        ]
      }
      """
      And I map the rule with ID "test3" to event "my-event" with weight 15
      And I create the rule:
      """
      {
        "actions": [
          { "id": "SetArg", "config": { "arg": "message", "value": "bar" } }
        ],
        "conditions": [
          { "id": "IsEqual", "config": { "actual": "@arg1", "expected": true } }
        ]
      }
      """
      And I map the rule with ID "test4" to event "my-event" with weight -10
      And I execute the event "my-event" with the following arguments:
      """
      { "arg1": true, "arg2": true }
      """
     Then The report should be:
      """
      {
        "event": "my-event",
        "group": null,
        "reports": [{
          "ruleId": "test4",
          "report": {
            "args": { "arg1": true, "arg2": true, "message": "bar" },
            "originalArgs": { "arg1": true, "arg2": true },
            "conditions": [{
              "args": { "arg1": true, "arg2": true },
              "condition": { "config": { "actual": "@arg1", "expected": true }, "id": "IsEqual" },
              "response": "pass"
            }],
            "actions": [{
              "action": { "config": { "arg": "message", "value": "bar" }, "id": "SetArg" },
              "args": { "arg1": true, "arg2": true },
              "response": "pass"
            }],
            "rule": {
              "conditions": [
                { "config": { "actual": "@arg1", "expected": true }, "id": "IsEqual" }
              ],
              "actions": [
                { "config": { "arg": "message", "value": "bar" }, "id": "SetArg" }
              ]
            },
            "success": true
          }
        }, {
          "ruleId": "test2",
          "report": {
            "args": { "arg1": true, "arg2": true, "message": "world" },
            "originalArgs": { "arg1": true, "arg2": true },
            "conditions": [{
              "args": { "arg1": true, "arg2": true },
              "condition": { "config": { "actual": "@arg1", "expected": true }, "id": "IsEqual" },
              "response": "pass"
            }],
            "actions": [{
              "action": { "config": { "arg": "message", "value": "world" }, "id": "SetArg" },
              "args": { "arg1": true, "arg2": true },
              "response": "pass"
            }],
            "rule": {
              "conditions": [
                { "config": { "actual": "@arg1", "expected": true }, "id": "IsEqual" }
              ],
              "actions": [
                { "config": { "arg": "message", "value": "world" }, "id": "SetArg" }
              ]
            },
            "success": true
          }
        }, {
          "ruleId": "test",
          "report": {
            "args": { "arg1": true, "arg2": true, "message": "hello" },
            "originalArgs": { "arg1": true, "arg2": true },
            "conditions": [{
              "args": { "arg1": true, "arg2": true },
              "condition": { "config": { "actual": "@arg1", "expected": true }, "id": "IsEqual" },
              "response": "pass"
            }],
            "actions": [{
              "action": { "config": { "arg": "message", "value": "hello" }, "id": "SetArg" },
              "args": { "arg1": true, "arg2": true },
              "response": "pass"
            }],
            "rule": {
              "conditions": [
                { "config": { "actual": "@arg1", "expected": true }, "id": "IsEqual" }
              ],
              "actions": [
                { "config": { "arg": "message", "value": "hello" }, "id": "SetArg" }
              ]
            },
            "success": true
          }
        }, {
          "ruleId": "test3",
          "report": {
            "args": { "arg1": true, "arg2": true, "message": "foo" },
            "originalArgs": { "arg1": true, "arg2": true },
            "conditions": [{
              "args": { "arg1": true, "arg2": true },
              "condition": { "config": { "actual": "@arg1", "expected": true }, "id": "IsEqual" },
              "response": "pass"
            }],
            "actions": [{
              "action": { "config": { "arg": "message", "value": "foo" }, "id": "SetArg" },
              "args": { "arg1": true, "arg2": true },
              "response": "pass"
            }],
            "rule": {
              "conditions": [
                { "config": { "actual": "@arg1", "expected": true }, "id": "IsEqual" }
              ],
              "actions": [
                { "config": { "arg": "message", "value": "foo" }, "id": "SetArg" }
              ]
            },
            "success": true
          }
        }]
      }
      """

  Scenario: Should map the rule and execute it via event
    Given I define the available conditions
      And I define the available actions
      And I register the failing action
     When I create the rule:
      """
      {
        "actions": [
          { "id": "SetArg", "config": { "arg": "message", "value": "hello" } }
        ],
        "conditions": [
          { "id": "IsEqual", "config": { "actual": "@arg1", "expected": true } }
        ]
      }
      """
      And I map the rule with ID "test" to event "my-event"
      And I create the rule:
      """
      {
        "actions": [
          { "id": "SetArg", "config": { "arg": "message", "value": "world" } }
        ],
        "conditions": [
          { "id": "IsEqual", "config": { "actual": "@arg1", "expected": true } }
        ]
      }
      """
      And I map the rule with ID "test2" to event "my-event" and group "group-2"
      And I create the rule:
      """
      {
        "actions": [
          { "id": "SetArg", "config": { "arg": "message", "value": "foo" } }
        ],
        "conditions": [
          { "id": "IsEqual", "config": { "actual": "@arg1", "expected": true } }
        ]
      }
      """
      And I map the rule with ID "test3" to event "my-event" and group "group-2" with weight 15
      And I create the rule:
      """
      {
        "actions": [
          { "id": "SetArg", "config": { "arg": "message", "value": "bar" } }
        ],
        "conditions": [
          { "id": "IsEqual", "config": { "actual": "@arg1", "expected": true } }
        ]
      }
      """
      And I map the rule with ID "test4" to event "my-event" and group "group-3" with weight -10
      And I execute the event "my-event" and group "group-2" with the following arguments:
      """
      { "arg1": true, "arg2": true }
      """
     Then The report should be:
      """
      {
        "event": "my-event",
        "group": "group-2",
        "reports": [{
          "ruleId": "test2",
          "report": {
            "args": { "arg1": true, "arg2": true, "message": "world" },
            "originalArgs": { "arg1": true, "arg2": true },
            "conditions": [{
              "args": { "arg1": true, "arg2": true },
              "condition": { "config": { "actual": "@arg1", "expected": true }, "id": "IsEqual" },
              "response": "pass"
            }],
            "actions": [{
              "action": { "config": { "arg": "message", "value": "world" }, "id": "SetArg" },
              "args": { "arg1": true, "arg2": true },
              "response": "pass"
            }],
            "rule": {
              "conditions": [
                { "config": { "actual": "@arg1", "expected": true }, "id": "IsEqual" }
              ],
              "actions": [
                { "config": { "arg": "message", "value": "world" }, "id": "SetArg" }
              ]
            },
            "success": true
          }
        }, {
          "ruleId": "test3",
          "report": {
            "args": { "arg1": true, "arg2": true, "message": "foo" },
            "originalArgs": { "arg1": true, "arg2": true },
            "conditions": [{
              "args": { "arg1": true, "arg2": true },
              "condition": { "config": { "actual": "@arg1", "expected": true }, "id": "IsEqual" },
              "response": "pass"
            }],
            "actions": [{
              "action": { "config": { "arg": "message", "value": "foo" }, "id": "SetArg" },
              "args": { "arg1": true, "arg2": true },
              "response": "pass"
            }],
            "rule": {
              "conditions": [
                { "config": { "actual": "@arg1", "expected": true }, "id": "IsEqual" }
              ],
              "actions": [
                { "config": { "arg": "message", "value": "foo" }, "id": "SetArg" }
              ]
            },
            "success": true
          }
        }]
      }
      """

  Scenario: I should be able to pass if conditions is missing
    Given I define the available conditions
      And I define the available actions
     When I create the rule:
      """
      {
        "actions": [{ "id": "SetArg", "config": { "arg": "message", "value": "hello" } }]
      }
      """
      And I execute the rule with the following arguments:
      """
      { "arg1": true }
      """
     Then The executed rule should have passed

