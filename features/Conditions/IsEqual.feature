@rules @conditions
Feature: Tests for the "IsEqual" condition

  Scenario: I should get an error if the condition config parameters are not available
    Given I define the available conditions
      And I have the defined condition "IsEqual"
     Then I execute the "IsEqual" condition with the following config and get the error "Missing the 'actual' and/or 'expected' properties in the config."
      """
      {}
      """

  Scenario: I should get a failed response if the values do not match
    Given I define the available conditions
      And I have the defined condition "IsEqual"
     Then I execute the "IsEqual" condition with the following config and fails
      """
      { "expected": "hello world", "actual": "foo bar" }
      """
      And I execute the "IsEqual" condition with the following args and config and fails
      """
      {
        "args": { "arg1": "hello world" },
        "config": { "expected": "@arg1", "actual": "foo bar" }
      }
      """

  Scenario: I should get a passed response if the values do match
    Given I define the available conditions
      And I have the defined condition "IsEqual"
     Then I execute the "IsEqual" condition with the following config and passes
      """
      { "expected": "hello world", "actual": "hello world" }
      """
      And I execute the "IsEqual" condition with the following config and passes
      """
      { "expected": { "hello": "world" }, "actual": { "hello": "world" } }
      """
      And I execute the "IsEqual" condition with the following args and config and passes
      """
      {
        "args": { "arg1": "hello world" },
        "config": { "expected": "@arg1", "actual": "hello world" }
      }
      """
      And I execute the "IsEqual" condition with the following args and config and passes
      """
      {
        "args": { "arg1": { "hello": "world" } },
        "config": { "expected": "@arg1", "actual": { "hello": "world" } }
      }
      """
