@rules @actions
Feature: Tests for the "SetArg" action

  Scenario: I should get an error if the action config parameters are not available
    Given I define the available actions
      And I have the defined action "SetArg"
     Then I execute the "SetArg" action with the following config and get the error "Missing the 'arg' and/or 'value' properties in the config."
      """
      {}
      """

  Scenario: I should get a passed response and updates the arguments
    Given I define the available actions
      And I have the defined action "SetArg"
     Then I execute the "SetArg" action with the following config and passes
      """
      { "arg": "arg1", "value": "hello" }
      """
      And I execute the "SetArg" action with the following args and config and passes
      """
      {
        "args": {},
        "config": { "arg": "arg1", "value": "hello world" },
        "postArgs": { "arg1": "hello world" }
      }
      """
      And I execute the "SetArg" action with the following args and config and passes
      """
      {
        "args": { "value": "hello world" },
        "config": { "arg": "arg1", "value": "@value" },
        "postArgs": { "value": "hello world", "arg1": "hello world" }
      }
      """
