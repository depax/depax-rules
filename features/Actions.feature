@rules @actions
Feature: Should be able to add, modify and remove action callbacks

  Scenario: I should be able to add a new action callback
    Given I have no defined actions
     When I add the test action callback
     Then I have the defined test action

  Scenario: I should be able to update an existing action callback
    Given I add the test action callback
     When I override the test action callback with another callback
     Then The defined test action has been overriden

  Scenario: I should be able to delete an existing action callback
    Given I add the test action callback
     When I delete the test action callback
     Then I have no defined actions
