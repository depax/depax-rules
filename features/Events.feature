@rules @events
Feature: Should be able to add, and remove events

  Scenario: I should be able to add a new event
    Given I have no defined events
     When I add the test event
     Then I have the defined test event

  Scenario: I should be able to delete an existing event
    Given I add the test event
     When I delete the test event
     Then I have no defined events
