@rules @conditions
Feature: Should be able to add, modify and remove condition callbacks

  Scenario: I should be able to add a new condition callback
    Given I have no defined conditions
     When I add the test condition callback
     Then I have the defined test condition

  Scenario: I should be able to update an existing condition callback
    Given I add the test condition callback
     When I override the test condition callback with another callback
     Then The defined test condition has been overriden

  Scenario: I should be able to delete an existing condition callback
    Given I add the test condition callback
     When I delete the test condition callback
     Then I have no defined conditions
