/**
 * Provides the test steps.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import chai = require("chai");
import { After, Before, Given, Then, When } from "cucumber";
import { actions, EResponse, ImportActions, IObject } from "../../src";

const TestAction = async (args: IObject, config: IObject): Promise<EResponse> => EResponse.Pass;
const TestAction2 = async (args: IObject, config: IObject): Promise<EResponse> => EResponse.Skip;
const FailingAction = async (args: IObject, config: IObject): Promise<EResponse> => EResponse.Fail;

After(async (): Promise<void> => {
    actions.clear();
});

Before(async (): Promise<void> => {
    actions.clear();
});

//#region ----< Define the *Given* steps >------------------------------------------------------------------------------
Given(/^I have no defined actions$/, async (): Promise<void> => {
    chai.assert.equal(actions.size, 0);
});

Given(/^I define the available actions$/, async (): Promise<void> => {
    await ImportActions();
});

Given(/^I have the defined action "(\S*)"$/, async (
    id: string,
): Promise<void> => {
    chai.assert.isTrue(actions.has(id));
});

Given(/^I register the failing action$/, async (): Promise<void> => {
    actions.set("FailingAction", { descriptor: null, callback: FailingAction });
});
//#endregion

//#region ----< Define the *When* steps >-------------------------------------------------------------------------------
When(/^I add the test action callback$/, async (): Promise<void> => {
    actions.set("test", { descriptor: null, callback: TestAction });
});

When(/^I override the test action callback with another callback$/, async (): Promise<void> => {
    actions.set("test", { descriptor: null, callback: TestAction2 });
});

When(/^I delete the test action callback$/, async (): Promise<void> => {
    actions.delete("test");
});
//#endregion

//#region ----< Define the *Then* steps >-------------------------------------------------------------------------------
Then(/^I have the defined test action$/, async (): Promise<void> => {
    chai.assert.equal(actions.size, 1);
    chai.assert.isTrue(actions.has("test"));
    chai.assert.equal(actions.get("test").callback, TestAction);
});

Then(/^The defined test action has been overriden$/, async (): Promise<void> => {
    chai.assert.equal(actions.size, 1);
    chai.assert.isTrue(actions.has("test"));
    chai.assert.equal(actions.get("test").callback, TestAction2);
});

Then(/^I execute the "(\S*)" action with the following config and (passes|fails)$/, async (
    id: string,
    state: string,
    payload: string,
): Promise<void> => {
    const response = await actions.get(id).callback({}, JSON.parse(payload));

    if (state === "passes") {
        state = EResponse.Pass;
    } else if (state === "fails") {
        state = EResponse.Fail;
    }

    chai.assert.equal(response, state);
});

Then(/^I execute the "(\S*)" action with the following args and config and (passes|fails)$/, async (
    id: string,
    state: string,
    payload: string,
): Promise<void> => {
    const input = JSON.parse(payload);
    const response = await actions.get(id).callback(input.args, input.config);

    if (state === "passes") {
        state = EResponse.Pass;
    } else if (state === "fails") {
        state = EResponse.Fail;
    }

    chai.assert.equal(response, state);

    if (input.postArgs) {
        chai.assert.deepEqual(input.args, input.postArgs);
    }
});

Then(/^I execute the "(\S*)" action with the following config and get the error "(.*)"$/, async (
    id: string,
    message: string,
    payload: string,
): Promise<void> => {
    try {
        await actions.get(id).callback({}, JSON.parse(payload));
        chai.assert.isTrue(false);
    } catch (err) {
        chai.assert.instanceOf(err, Error);
        chai.assert.propertyVal(err, "message", message);
    }
});
//#endregion
