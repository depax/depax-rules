/**
 * Provides the test steps.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import chai = require("chai");
import { After, Before, Given, Then, When } from "cucumber";
import { events, IEvent } from "../../src";

const TestEvent: IEvent = { title: "test", description: "Test event.", category: "tests" };

After(async (): Promise<void> => {
    events.clear();
});

Before(async (): Promise<void> => {
    events.clear();
});

//#region ----< Define the *Given* steps >------------------------------------------------------------------------------
Given(/^I have no defined events$/, async (): Promise<void> => {
    chai.assert.equal(events.size, 0);
});
//#endregion

//#region ----< Define the *When* steps >-------------------------------------------------------------------------------
When(/^I add the test event$/, async (): Promise<void> => {
    events.set("test", TestEvent);
});

When(/^I delete the test event$/, async (): Promise<void> => {
    events.delete("test");
});
//#endregion

//#region ----< Define the *Then* steps >-------------------------------------------------------------------------------
Then(/^I have the defined test event$/, async (): Promise<void> => {
    chai.assert.equal(events.size, 1);
    chai.assert.isTrue(events.has("test"));
    chai.assert.equal(events.get("test"), TestEvent);
});
//#endregion
