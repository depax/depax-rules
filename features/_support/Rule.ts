/**
 * Provides the test steps.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import chai = require("chai");
import { Before, Then, When } from "cucumber";
import Execute, { ExecuteEvent, IEventReport, IObject, IReport, IRule, mappings, MapRule, rules } from "../../src";

let rule: IRule;
let args: IObject;
let report: IReport | IEventReport;

Before(async (): Promise<void> => {
    rules.clear();
    mappings.clear();
});

//#region ----< Define the *When* steps >-------------------------------------------------------------------------------
When(/^I create the rule:$/, async (
    payload: string,
): Promise<void> => {
    rule = JSON.parse(payload);
});

When(/^I map the rule with ID "(\S*)" to event "(\S*)"$/, async (
    ruleId: string,
    event: string,
): Promise<void> => {
    rules.set(ruleId, rule);
    MapRule(ruleId, event);
});

When(/^I map the rule with ID "(\S*)" to event "(\S*)" with weight ([\-0-9]*)$/, async (
    ruleId: string,
    event: string,
    weight: number,
): Promise<void> => {
    rules.set(ruleId, rule);
    MapRule(ruleId, event, weight);
});

When(/^I map the rule with ID "(\S*)" to event "(\S*)" and group "(\S*)"$/, async (
    ruleId: string,
    event: string,
    group: string,
): Promise<void> => {
    rules.set(ruleId, rule);
    MapRule(ruleId, event, 0, group);
});

When(/^I map the rule with ID "(\S*)" to event "(\S*)" and group "(\S*)" with weight ([\-0-9]*)$/, async (
    ruleId: string,
    event: string,
    group: string,
    weight: number,
): Promise<void> => {
    rules.set(ruleId, rule);
    MapRule(ruleId, event, weight, group);
});

When(/^I execute the rule with the following arguments:$/, async (
    payload: string,
): Promise<void> => {
    args = JSON.parse(payload);
    report = await Execute(rule, args);
});

When(/^I execute the event "(\S*)" with the following arguments:$/, async (
    event: string,
    payload: string,
): Promise<void> => {
    args = JSON.parse(payload);
    report = await ExecuteEvent(event, args);
});

When(/^I execute the event "(\S*)" and group "(\S*)" with the following arguments:$/, async (
    event: string,
    group: string,
    payload: string,
): Promise<void> => {
    args = JSON.parse(payload);
    report = await ExecuteEvent(event, args, group);
});
//#endregion

//#region ----< Define the *Then* steps >-------------------------------------------------------------------------------
Then(/^The executed rule should have (passed|failed)$/, async (
    state: string,
): Promise<void> => {
    chai.assert[state === "passed" ? "isTrue" : "isFalse"]((report as IReport).success);
});

Then(/^The report should be:$/, async (
    payload: string,
): Promise<void> => {
    chai.assert.deepEqual(report, JSON.parse(payload));
});

Then(/^The arguments should be:$/, async (
    payload: string,
): Promise<void> => {
    chai.assert.deepEqual(args, JSON.parse(payload));
});
//#endregion
