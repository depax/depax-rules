/**
 * Provides the test steps.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import chai = require("chai");
import { After, Before, Given, Then, When } from "cucumber";
import { conditions, EResponse, ImportConditions, IObject } from "../../src";

const TestCondition = async (args: IObject, config: IObject): Promise<EResponse> => EResponse.Pass;
const TestCondition2 = async (args: IObject, config: IObject): Promise<EResponse> => EResponse.Skip;

After(async (): Promise<void> => {
    conditions.clear();
});

Before(async (): Promise<void> => {
    conditions.clear();
});

//#region ----< Define the *Given* steps >------------------------------------------------------------------------------
Given(/^I have no defined conditions$/, async (): Promise<void> => {
    chai.assert.equal(conditions.size, 0);
});

Given(/^I define the available conditions$/, async (): Promise<void> => {
    await ImportConditions();
});

Given(/^I have the defined condition "(\S*)"$/, async (
    id: string,
): Promise<void> => {
    chai.assert.isTrue(conditions.has(id));
});
//#endregion

//#region ----< Define the *When* steps >-------------------------------------------------------------------------------
When(/^I add the test condition callback$/, async (): Promise<void> => {
    conditions.set("test", { descriptor: null, callback: TestCondition });
});

When(/^I override the test condition callback with another callback$/, async (): Promise<void> => {
    conditions.set("test", { descriptor: null, callback: TestCondition2 });
});

When(/^I delete the test condition callback$/, async (): Promise<void> => {
    conditions.delete("test");
});
//#endregion

//#region ----< Define the *Then* steps >-------------------------------------------------------------------------------
Then(/^I have the defined test condition$/, async (): Promise<void> => {
    chai.assert.equal(conditions.size, 1);
    chai.assert.isTrue(conditions.has("test"));
    chai.assert.equal(conditions.get("test").callback, TestCondition);
});

Then(/^The defined test condition has been overriden$/, async (): Promise<void> => {
    chai.assert.equal(conditions.size, 1);
    chai.assert.isTrue(conditions.has("test"));
    chai.assert.equal(conditions.get("test").callback, TestCondition2);
});

Then(/^I execute the "(\S*)" condition with the following config and (passes|fails)$/, async (
    id: string,
    state: string,
    payload: string,
): Promise<void> => {
    const response = await conditions.get(id).callback({}, JSON.parse(payload));

    if (state === "passes") {
        state = EResponse.Pass;
    } else if (state === "fails") {
        state = EResponse.Fail;
    }

    chai.assert.equal(response, state);
});

Then(/^I execute the "(\S*)" condition with the following args and config and (passes|fails)$/, async (
    id: string,
    state: string,
    payload: string,
): Promise<void> => {
    const input = JSON.parse(payload);
    const response = await conditions.get(id).callback(input.args, input.config);

    if (state === "passes") {
        state = EResponse.Pass;
    } else if (state === "fails") {
        state = EResponse.Fail;
    }

    chai.assert.equal(response, state);
});

Then(/^I execute the "(\S*)" condition with the following config and get the error "(.*)"$/, async (
    id: string,
    message: string,
    payload: string,
): Promise<void> => {
    try {
        await conditions.get(id).callback({}, JSON.parse(payload));
        chai.assert.isTrue(false);
    } catch (err) {
        chai.assert.instanceOf(err, Error);
        chai.assert.propertyVal(err, "message", message);
    }
});
//#endregion
